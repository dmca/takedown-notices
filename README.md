# Takedown Notices

This repository stores all DMCA takedown notices by request. Please refer to our [DMCA Takedown Policy](https://www.codelinaro.org/policies/dmca-takedown-policy.html) for further information.

~ CodeLinaro Support
